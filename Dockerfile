FROM centos:7
LABEL Name=lead Version=0.0.1 maintainer="Michael Daffin <michael@daffin.io>"
RUN yum install -y rpm-build
RUN yum groupinstall -y "development tools"
RUN useradd -ms /bin/bash lead
COPY target/x86_64-unknown-linux-musl/debug/lead /usr/bin/lead
CMD lead
USER lead
