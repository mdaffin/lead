use std::path::{Path, PathBuf};
use std::fs;
use std::process::Command;
use std::io::{BufWriter, Write};
use std::fs::File;
use glob::glob;
use super::Sources;
use errors::*;

static SPEC_TEMPLATE: &'static str = include_str!("./templates/rpm.spec");

#[derive(Serialize, Deserialize, Debug)]
pub struct Output {
    name: String,
    description: Option<String>,
    version: Option<String>,
    release: Option<String>,
    summary: Option<String>,
    license: Option<String>,
    icon: Option<String>,
    vendor: Option<String>,
    packager: Option<String>,
    group: Option<String>,
    #[serde(rename = "long-description")]
    long_description: Option<String>,
    #[serde(default)]
    depends: Vec<String>,
    #[serde(default)]
    sources: Sources,
}

impl Output {
    pub fn build_dir<P: AsRef<Path>>(&self, build_root: P) -> Result<PathBuf> {
        let br = build_root.as_ref().join("BUILD");
        if !br.exists() {
            fs::create_dir(br.to_owned()).chain_err(|| format!("failed to create `{}`", br.display()))?;
        }
        Ok(br)
    }

    pub fn build<P: AsRef<Path>>(&self, top_dir: P) -> Result<()> {
        let build_dir = self.build_dir(top_dir.as_ref())?;
        let sources_dir = top_dir.as_ref().join("SOURCES");
        let rpms_dir = top_dir.as_ref().join("RPMS");
        let specs_dir = top_dir.as_ref().join("SPECS");

        for dir in vec![sources_dir.to_owned(), rpms_dir.to_owned(), specs_dir.to_owned()] {
            fs::create_dir(dir.to_owned()).chain_err(|| { format!("failed to create `{}`", dir.display()) })?;
        }

        let spec_file = self.write_spec_file(specs_dir.to_owned())?;

        let output = Command::new("rpmbuild").arg("-bb")
            .arg(format!("--define=_topdir {}", top_dir.as_ref().to_string_lossy()))
            .arg(format!("--define=buildroot {}", build_dir.to_string_lossy()))
            .arg(spec_file)
            .output()
            .chain_err(|| "failed to execute rpmbuild")?;
        println!("{}", String::from_utf8(output.stdout).unwrap());
        println!("{}", String::from_utf8(output.stderr).unwrap());

        let pattern = &rpms_dir.join("**/*.rpm");
        for entry in glob(pattern.to_str()
                .expect("invalid unicode in build path"))
            .expect("failed reading glob pattern") {
            let src = entry.unwrap();
            let dest = src.to_owned();
            let dest = dest.file_name().unwrap();

            println!("Copying `{}` to `{}`",
                     src.to_string_lossy(),
                     dest.to_string_lossy());
            fs::copy(src, dest).expect("failed to move source");

        }
        Ok(())
    }

    fn write_spec_file<P: AsRef<Path>>(&self, spec_dir: P) -> Result<PathBuf> {
        use tera::{Tera, Context};

        let mut context = Context::new();
        context.add("package", &self);
        let output = Tera::one_off(SPEC_TEMPLATE, context, false)
            .expect("failed to parse inbuilt template");

        println!("{}", output.to_owned());

        let filename = spec_dir.as_ref().join(format!("{}.spec", self.name));

        let mut file = BufWriter::new(File::create(filename.to_owned()).chain_err(|| {
                format!("failed open `{}` for writing", filename.to_string_lossy())
            })?);

        write!(file, "{}", output).chain_err(|| {
                format!("failed writing to `{}`", filename.to_string_lossy())
            })?;

        Ok(filename)
    }
}
