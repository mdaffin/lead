%define __spec_prep_post true
%define __spec_prep_pre true
%define __spec_build_post true
%define __spec_build_pre true
%define __spec_install_post true
%define __spec_install_pre true
%define __spec_clean_post true
%define __spec_clean_pre true

Name:     {{ package.name }}
{% if package.icon %}Icon:     {{ package.icon }}{% endif %}
{% if package.version %}Version:  {{ package.version }}{% endif %}
{% if package.release %}Release:  {{ package.release }}{% endif %}
{% if package.summary %}Summary:  {{ package.summary }}{% endif %}
{% if package.license %}License:  {{ package.license }}{% endif %}
{% if package.vendor %}Vendor:   {{ package.vendor }}{% endif %}
{% if package.packager %}Packager: {{ package.packager }}{% endif %}
{% if package.group %}Group:    {{ package.group }}{% endif %}


%description
{{ package.description }}

%files
{% for source in package.sources %}
{% if source.file is defined %}
{{ source.file }}
{% elif source.dir is defined %}
%dir {{ source.dir }}
{% elif source.config is defined %}
%config {{ source.config }}
{% elif source.doc is defined %}
%doc {{ source.config }}
{% endif %}
{% endfor %}

%changelog
