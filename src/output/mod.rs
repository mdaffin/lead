mod rpm;

#[cfg(test)]
mod tests;

use std::path::{PathBuf, Path};
use std::fs::File;
use serde_yaml;
use errors::*;

#[derive(Serialize, Deserialize, Debug)]
pub enum Output {
    #[serde(rename = "rpm")]
    RPM(rpm::Output),
}

impl Output {
    /// Creates and returns the directoy to use as the DESTDIR during building.
    /// This directoy will be packaged up by the given output module.
    pub fn build_dir<P: AsRef<Path>>(&self, build_root: P) -> Result<PathBuf> {
        match *self {
            Output::RPM(ref o) => o.build_dir(build_root),
        }
    }

    /// Build the package for the given output module.
    pub fn build<P: AsRef<Path>>(&self, build_dir: P) -> Result<()> {
        match *self {
            Output::RPM(ref o) => o.build(build_dir).chain_err(|| "failed to build rpm"),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum Source {
    #[serde(rename = "file")]
    File(String),
    #[serde(rename = "dir")]
    Dir(String),
    #[serde(rename = "config")]
    Config(String),
    #[serde(rename = "doc")]
    Doc(String),
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Sources(Vec<Source>);

impl Default for Sources {
    fn default() -> Sources {
        Sources(Vec::new())
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct OutputsWrapper {
    outputs: Vec<Output>,
}

pub fn load_conf<P: AsRef<Path>>(config: P) -> Result<Vec<Output>> {
    let f = File::open(config.as_ref()).chain_err(|| {
            format!("failed opening config `{}`",
                    config.as_ref().to_string_lossy())
        })?;
    let wrapper: OutputsWrapper = serde_yaml::from_reader(&f)
        .chain_err(|| format!("failed to parse `{}`", config.as_ref().to_string_lossy()))?;
    Ok(wrapper.outputs)
}