use super::*;
use tempdir::TempDir;
use std::env::{current_dir, set_current_dir};
use std::path::PathBuf;
use walkdir::WalkDir;
use std::collections::HashSet;

fn load_test_data<P: AsRef<Path>>(filename: P) -> Vec<Test> {
    let f = File::open(filename.as_ref()).expect("failed to open test yml");
    serde_yaml::from_reader(&f).expect("failed to parse test yml")
}

#[derive(Serialize, Deserialize, Debug)]
struct Test {
    message: String,
    input: Input,
    expected: HashSet<PathBuf>,
    build_err: Option<String>,
    install_err: Option<String>,
}

#[test]
fn test_input_build_and_install() {
    let test_dir =
        current_dir().expect("failed to get the project root").join("src/input/tests/data");
    set_current_dir(test_dir).expect("failed to change to test dir");

    for test in load_test_data("tests.yml") {
        println!("running example `{}`", test.message);
        let dest_dir = TempDir::new("lead_test_").expect("failed to create temp dir");

        {
            let build_result = test.input.build(true);
            match test.build_err {
                Some(exp_err) => {
                    match build_result {
                        Ok(()) => panic!("expected error, non found"),
                        Err(build_err) => {
                            assert_eq!(exp_err, build_err.to_string());
                            continue;
                        }
                    }
                }
                None => build_result.expect("failed to run build"),
            }
        }
        {
            let install_result = test.input.install(dest_dir.path(), true);
            match test.install_err {
                Some(exp_err) => {
                    match install_result {
                        Ok(()) => panic!("expected error, non found"),
                        Err(install_err) => {
                            assert_eq!(exp_err, install_err.to_string());
                            continue;
                        }
                    }
                }
                None => install_result.expect("failed to run install"),
            }
        }

        let sources: HashSet<PathBuf> = WalkDir::new(dest_dir.path())
            .into_iter()
            .map(|e| e.unwrap())
            .map(|e| e.path().to_path_buf())
            .map(|e| e.strip_prefix(dest_dir.path()).unwrap().to_path_buf())
            .filter(|e| e.as_os_str() != "")
            .collect();

        assert_eq!(test.expected, sources);
    }
}