//! Custom script module, runs a user specified build and install script that
//! are used to prep the build directory for the given output format.

use std::process::{Command, Stdio};
use errors::*;
use std::path::Path;

#[derive(Serialize, Deserialize, Debug)]
pub struct Script {
    build: Option<Cmd>,
    install: Cmd,
}

impl Script {
    pub fn build(&self, verbose: bool) -> Result<()> {
        match self.build {
            Some(ref b) => b.run(None, verbose),
            None => Ok(()),
        }
    }
    pub fn install<P: AsRef<Path>>(&self, build_dir: P, verbose: bool) -> Result<()> {
        self.install.run(Some(build_dir.as_ref()), verbose)?;
        Ok(())
    }
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
enum Cmd {
    Inline(String),
    List(Vec<String>),
}

impl Cmd {
    pub fn run_command(command: &str, build_dir: Option<&Path>, verbose: bool) -> Result<()> {
        let stdio = if verbose {
            println!("$ {}", command);
            Stdio::inherit
        } else {
            Stdio::null
        };
        let mut cmd = Command::new("sh");
        cmd.arg("-c")
            .arg(command)
            .stdin(Stdio::null())
            .stdout(stdio())
            .stderr(stdio());
        match build_dir {
            Some(d) => {
                cmd.env("DESTDIR", d);
            }
            None => (),
        };
        let status = cmd.status().chain_err(|| format!("failed to run command `{}`", command))?;
        if !status.success() {
            bail!(format!("command exited unsuccessfully with exit status `{}`",
                          status))
        } else {
            Ok(())
        }
    }

    pub fn run(&self, build_dir: Option<&Path>, verbose: bool) -> Result<()> {
        match *self {
            Cmd::Inline(ref command) => Cmd::run_command(&command, build_dir, verbose),
            Cmd::List(ref list) => {
                for command in list {
                    Cmd::run_command(&command, build_dir, verbose)?;
                }
                Ok(())
            }
        }
    }
}