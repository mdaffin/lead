mod script;
mod dir;

#[cfg(test)]
mod tests;

use errors::*;
use std::path::Path;
use std::fs::File;
use serde_yaml;

#[derive(Serialize, Deserialize, Debug)]
#[serde(tag = "type")]
pub enum Input {
    #[serde(rename = "script")]
    Script(script::Script),
    #[serde(rename = "dir")]
    Directory(dir::Directory),
}

impl Input {
    pub fn build(&self, verbose: bool) -> Result<()> {
        match *self {
            Input::Script(ref i) => i.build(verbose),
            Input::Directory(_) => Ok(()),
        }
    }

    pub fn install<P: AsRef<Path>>(&self, build_dir: P, verbose: bool) -> Result<()> {
        match *self {
            Input::Script(ref i) => i.install(build_dir, verbose),
            Input::Directory(ref i) => i.install(build_dir, verbose),
        }
    }
}

pub fn load_conf<P: AsRef<Path>>(config: P) -> Result<Input> {
    let f = File::open(config.as_ref()).chain_err(|| {
            format!("failed opening config `{}`",
                    config.as_ref().to_string_lossy())
        })?;
    serde_yaml::from_reader(&f)
        .chain_err(|| format!("failed to parse `{}`", config.as_ref().to_string_lossy()))
}
