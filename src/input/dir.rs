//! Copies the contents of a directoy to the build directory for each output.
//! This input module is useful if you have an external build process and just
//! want to package the output.

use errors::*;
use std::path::{Path, PathBuf};
use walkdir::WalkDir;
use std::fs;

/// Directory input module, copies a directory to the build directory of each
/// output. Optionally adds a prefix to the build dir.
#[derive(Serialize, Deserialize, Debug)]
pub struct Directory {
    path: PathBuf,
    prefix: Option<PathBuf>,
}

impl Directory {
    pub fn install<P: AsRef<Path>>(&self, dest: P, verbose: bool) -> Result<()> {
        for entry in WalkDir::new(self.path.clone()) {
            let entry = entry.chain_err(|| "could not copy source directory")?;
            let src_path = entry.path();
            let dest_path = src_path.strip_prefix(self.path.as_os_str())
                .expect("subdir or file not inside path");

            let dest = match self.prefix {
                Some(ref p) if p.starts_with("/") => {
                    dest.as_ref().join(p.strip_prefix("/").unwrap())
                }
                Some(ref p) => dest.as_ref().join(p),
                None => dest.as_ref().to_path_buf(),
            };

            let dest_path = dest.join(dest_path);

            if src_path.is_dir() {
                if verbose {
                    println!("Creating directory `{}`", dest_path.clone().display());
                }
                fs::create_dir_all(dest_path.clone()).chain_err(|| format!("could not create directoy `{}`", dest_path.display()))?;
            } else {
                if verbose {
                    println!("Copying file `{}`", dest_path.clone().display());
                    fs::copy(src_path, dest_path)
                        .chain_err(|| format!("failed to copy `{}`", src_path.display()))?;
                }
            }
        }
        Ok(())
    }
}