#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate serde_yaml;
extern crate tempdir;
extern crate walkdir;
extern crate tera;
extern crate glob;

mod errors;
mod input;
mod output;

use std::path::Path;
use std::io::Write;
use error_chain::ChainedError; // trait which holds `display`
use tempdir::TempDir;
use errors::*;

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    input: input::Input,
    outputs: Vec<output::Output>,
}

fn tree<P: AsRef<Path>>(dir: P) {
    use walkdir::WalkDir;
    for entry in WalkDir::new(dir) {
        let entry = entry.unwrap();
        println!("{}", entry.path().display());
    }
}

macro_rules! print_err {
    ($x:expr) => {
        $x.unwrap_or_else(|e| {
            let stderr = &mut ::std::io::stderr();
            writeln!(stderr, "{}", e.display()).unwrap();
            ::std::process::exit(1);
        })
    }
}

fn main() {
    let ip = print_err!(input::load_conf(".lead.yml"));
    let op = print_err!(output::load_conf(".lead.yml"));
    print_err!(ip.build(true));

    for output in op {
        let build_root = TempDir::new("lead_").expect("failed to create temp dir");
        let build_dir = print_err!(output.build_dir(build_root.path()));
        print_err!(ip.install(build_dir, true));
        print_err!(output.build(build_root.path()));
        tree(build_root.path());
        let display_path = build_root.path().to_owned();
        print_err!(build_root.close().chain_err(|| {
            format!("failed to delete build directory `{}`",
                    display_path.display())
        }));
    }
}
