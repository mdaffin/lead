# Lead

A package building tool.

## Testing in a docker image

Install the `x86_64-unknown-linux-musl` target to allow compiling against a
static libc and build the base docker image.

```
rustup target install x86_64-unknown-linux-musl
docker build -f Dockerfile -t lead:latest .
```

Staticly compile and run inside docker.

```
cargo build --target x86_64-unknown-linux-musl
sudo docker run --rm -it -v $PWD/target/x86_64-unknown-linux-musl/debug/lead:/usr/bin/lead -v $PWD:/code -w /code lead
```
